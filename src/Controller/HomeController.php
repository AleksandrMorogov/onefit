<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, AuthorizationCheckerInterface $aci)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAll();
        return $this->render('home/index.html.twig', [
            'currentUser'=> $this->getUser(),
            'users'=> $users,
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/newComment", name="newComment")
     */
    public function newComment(Request $request)
    {
        $text = $request->request->get("commentText", "Не пошло");
        $comment = new Comment();
        $comment->setUser($this->getUser());
        $comment->setText($text);
        $man = $this->getDoctrine()->getManager();
        $man->persist($comment);
        $man->flush();
        return new Response("OK");
    }

    /**
     * @Route("/editComment", name="editComment")
     */
    public function editComment(Request $request)
    {
        $text = $request->request->get("text", "Попортил коммент");
        $id = $request->request->get("id");
        $comment = $this->getDoctrine()->getRepository(Comment::class)->find($id);
        $comment->setText($text);
        $man = $this->getDoctrine()->getManager();
        $man->flush();
        return new Response("OK");
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile()
    {
        return $this->render("home/profile.html.twig", [ 'currentUser' => $this->getUser() ]);
    }

    /**
     * @Route("/editProfile", name="editProfile")
     */
    public function editProfile(Request $request)
    {
        $firstName = $request->request->get("firstName");
        $lastName = $request->request->get("lastName");
        $curUser = $this->getUser();
        $curUser->setFirstName($firstName);
        $curUser->setLastName($lastName);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($curUser);
        $manager->flush();
        return $this->redirect('/');
    }

    /**
     * @Route("/reg", name="reg")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $man = $this->getDoctrine()->getManager();
            $man->persist($user);
            $man->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render("home/register.html.twig", [ 'form' => $form->createView() ]);
    }
}
