jQuery(document).ready(function() {

    $('.modal-footer').on('click', '#newCommentButton', function (data) {
        $.ajax({
            url: '/newComment',
            method: 'POST',
            data: {
                commentText: $('#commentText')[0].value
            }
        }).done(function(){
            $('#newCommentModal').hide();
            $('.modal-backdrop').hide();
            location.reload();
        });
    })

    $('.comment-block').on('click', '.button-edit', function (data) {
        var commentText = data.target.parentNode.childNodes[1];
        var parentNode = data.target.parentNode;
        var id = $(parentNode).data('id');
        $(commentText).replaceWith('<input type="text" value="' + commentText.textContent +
            '"><button type="button" onclick="editComment(' + id + ')">Ok</button>');
    })

});

function editComment(id) {
    var inputText = $(".comment-block[data-id='" + id + "']")[0].childNodes[1];
    var btn = $(".comment-block[data-id='" + id + "']")[0].childNodes[2];
    var newComment = inputText.value;
    $.ajax({
        url: "/editComment",
        method: "POST",
        data: {
            id: id,
            text: newComment
        }
    }).done(function () {
        $(inputText).replaceWith('<p class="comment-text">' + newComment + '</p>');
        btn.replaceWith('');
    });

}